package ru.vmaksimenkov.tm.constant;

public class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_VERSION = "version";
    
}
